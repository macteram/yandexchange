<?php
/*
=====================================================
 Вывод объектов на карте по категориям
-----------------------------------------------------
 Сopyright (c) 2020
=====================================================
 Данный код защищен авторскими правами
=====================================================
*/
if(!defined('DATALIFEENGINE'))
{
  die("Hacking attempt!");
}

function main() {
global $db, $tpl, $metatags, $config, $lang, $is_logged, $member_id, $user_group;
//получение массива адресов из БД
$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_adress != '' ORDER BY id ASC");
$i=0;

while ($row = $db->get_row($tb))
	{
		$List[] = $row;
	}	

foreach($List as $row){
//Получение данных с геокодера
		$get_coor = $homepage = file_get_contents('https://geocode-maps.yandex.ru/1.x/?apikey=0e041a51-3941-4731-8143-3e8b36d4c073&format=json&geocode=' . urlencode( $row['ca_adress'] ) . '&results=1');
	
		$decode_coor = json_decode( $get_coor, true );
		
		$coord = $decode_coor['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
		$coord_de = explode(' ', $coord);
		$coord = "{$coord_de[1]}, {$coord_de[0]}";
		
		if( ! $first_point ) $first_point = $coord;
		
		/*подгружаем фотографии категории*/
		$cdir = scandir(ROOT_DIR . "/uploads/category/".$row['alt_name']); 
	   
		foreach ($cdir as $key => $value) 
		{ 
		  if (!in_array($value,array(".","..","Thumbs.db"))) 
		  { 
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
			 { 
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			 } 
			 else 
			 { 
				$result[] = $value; 
			 } 
		  } 
		} 
		
		$kid = 1;
		$previmg = '';
		
		foreach( $result as $img )
		{
			$exname = explode('.', $img);
			
			if( $exname[0] == 'main' )
			{
				//$tpl = str_replace('{image-1}', '/uploads/category/' . $row['alt_name'] . '/' . $img );
			}
			
			$kid += 1;
			
			
			if( $kid >= 1 )
			{
				$previmg .= '<img src="/uploads/category/' . $row['alt_name'] . '/' . $img . '" alt="">';
			}
		}
		
		# Рейтинг категории
		#
		$ResultSum = $db->super_query( "SELECT sum(rating) as `sum` FROM " . USERPREFIX . "_post
							LEFT JOIN " . USERPREFIX . "_post_extras ON " . USERPREFIX . "_post.id=" . USERPREFIX . "_post_extras.news_id
							WHERE " . USERPREFIX . "_post.category = '" . $row['id'] . "'" );	
		
		$rating = intval($ResultSum['sum']);


	
		$maps .= "
		
		var myPlacemark = new ymaps.Placemark([{$coord}], 
{
balloonContentHeader: \"<div class='maps-balloon'><table><tbody><tr><td class='details'><img src='/uploads/category/" . $row['alt_name'] . "/". $img. "'><div class='titles'><a href='{$row['alt_name']}' target='_top'>{$row['name']}</a></div><div class='address'>{$rating}</div>\" ,
					balloonContentBody: \"<div class='contacts'><li style='list-style: none;' class='phone'></li></ul></div></div></td></tr></tbody></table></div>\",
								},

{
					draggable: false,
					iconLayout: 'default#image',
					iconImageHref: '{THEME}/images/map.png',
					iconImageSize: [32, 37],
					iconImageOffset: [-3, -42]
				});

myGeoObjects.push(myPlacemark);
		";
		$i++;	
		
	}
	
$title = "Интерактивная карта &raquo; все салоны";
$metatags['title'] = $title;

$tpl->load_template('maps.tpl');
$db->free();
$tpl->set('{maps}',$maps);
$tpl->set('{first_point}',$first_point);
$tpl->compile('content');
$tpl->clear();

}


function cat1() {
global $db, $tpl, $metatags, $config, $lang, $is_logged, $member_id, $user_group;

//$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_parentid IN (1) ORDER BY id");
$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_adress != '' AND ca_parentid IN (1) ORDER BY id");
//$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_adress != '' ORDER BY id ASC");

//$sql_result = $db->query("SELECT categories.cat_name, firms.fid, firms.name, firms.adress, firms.logo, firms.geo, firms.hour, firms.ontime, firms.offtime, firms.ontime, firms.offtime, firms.cat_id, firms.mapicon, firms.altname FROM " . PREFIX . "_firms_categories as categories, " . PREFIX . "_firms as firms WHERE firms.cat_id = categories.cid AND firms.cat_id IN (1,2,3,4,5,6,7,8,9) ORDER BY firms.fid");
$i=0;

while ($row = $db->get_row($tb))
	{
		$List[] = $row;
	}	

foreach($List as $row){

		$get_coor = $homepage = file_get_contents('https://geocode-maps.yandex.ru/1.x/?apikey=0e041a51-3941-4731-8143-3e8b36d4c073&format=json&geocode=' . urlencode( $row['ca_adress'] ) . '&results=1');
	
		$decode_coor = json_decode( $get_coor, true );
		
		$coord = $decode_coor['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
		$coord_de = explode(' ', $coord);
		$coord = "{$coord_de[1]}, {$coord_de[0]}";
		
		if( ! $first_point ) $first_point = $coord;
		
		/*подгружаем фотографии категории*/
		$cdir = scandir(ROOT_DIR . "/uploads/category/".$row['alt_name']); 
	   
		foreach ($cdir as $key => $value) 
		{ 
		  if (!in_array($value,array(".","..","Thumbs.db"))) 
		  { 
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
			 { 
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			 } 
			 else 
			 { 
				$result[] = $value; 
			 } 
		  } 
		} 
		
		$kid = 1;
		$previmg = '';
		
		foreach( $result as $img )
		{
			$exname = explode('.', $img);
			
			if( $exname[0] == 'main' )
			{
				//$tpl = str_replace('{image-1}', '/uploads/category/' . $row['alt_name'] . '/' . $img );
			}
			
			$kid += 1;
			
			
			if( $kid >= 1 )
			{
				$previmg .= '<img src="/uploads/category/' . $row['alt_name'] . '/' . $img . '" alt="">';
			}
		}
		
		# Рейтинг категории
		#
		$ResultSum = $db->super_query( "SELECT sum(rating) as `sum` FROM " . USERPREFIX . "_post
							LEFT JOIN " . USERPREFIX . "_post_extras ON " . USERPREFIX . "_post.id=" . USERPREFIX . "_post_extras.news_id
							WHERE " . USERPREFIX . "_post.category = '" . $row['id'] . "'" );	
		
		$rating = intval($ResultSum['sum']);


	
		$maps .= "
		
		var myPlacemark = new ymaps.Placemark([{$coord}], 
{
balloonContentHeader: \"<div class='maps-balloon'><table><tbody><tr><td class='details'><img src='/uploads/category/" . $row['alt_name'] . "/". $img. "'><div class='titles'><a href='{$row['alt_name']}' target='_top'>{$row['name']}</a></div><div class='address'>{$rating}</div>\" ,
					balloonContentBody: \"<div class='contacts'><li style='list-style: none;' class='phone'></li></ul></div></div></td></tr></tbody></table></div>\",
								},

{
					draggable: false,
					iconLayout: 'default#image',
					iconImageHref: '{THEME}/images/map.png',
					iconImageSize: [32, 37],
					iconImageOffset: [-3, -42]
				});

myGeoObjects.push(myPlacemark);
		";
		$i++;	
		
	}
	
$title = "Интерактивная карта &raquo; женские залы";
$metatags['title'] = $title;

$tpl->load_template('maps.tpl');
$db->free();
$tpl->set('{maps}',$maps);
$tpl->set('{first_point}',$first_point);
$tpl->compile('content');
$tpl->clear();

}

function cat2() {
global $db, $tpl, $metatags, $config, $lang, $is_logged, $member_id, $user_group;

$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_parentid1 IN (2) ORDER BY id");
$i=0;

while ($row = $db->get_row($tb))
	{
		$List[] = $row;
	}	

foreach($List as $row){

		$get_coor = $homepage = file_get_contents('https://geocode-maps.yandex.ru/1.x/?apikey=0e041a51-3941-4731-8143-3e8b36d4c073&format=json&geocode=' . urlencode( $row['ca_adress'] ) . '&results=1');
	
		$decode_coor = json_decode( $get_coor, true );
		
		$coord = $decode_coor['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
		$coord_de = explode(' ', $coord);
		$coord = "{$coord_de[1]}, {$coord_de[0]}";
		
		if( ! $first_point ) $first_point = $coord;
		
		/*подгружаем фотографии категории*/
		$cdir = scandir(ROOT_DIR . "/uploads/category/".$row['alt_name']); 
	   
		foreach ($cdir as $key => $value) 
		{ 
		  if (!in_array($value,array(".","..","Thumbs.db"))) 
		  { 
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
			 { 
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			 } 
			 else 
			 { 
				$result[] = $value; 
			 } 
		  } 
		} 
		
		$kid = 1;
		$previmg = '';
		
		foreach( $result as $img )
		{
			$exname = explode('.', $img);
			
			if( $exname[0] == 'main' )
			{
				//$tpl = str_replace('{image-1}', '/uploads/category/' . $row['alt_name'] . '/' . $img );
			}
			
			$kid += 1;
			
			
			if( $kid >= 1 )
			{
				$previmg .= '<img src="/uploads/category/' . $row['alt_name'] . '/' . $img . '" alt="">';
			}
		}
		
		# Рейтинг категории
		#
		$ResultSum = $db->super_query( "SELECT sum(rating) as `sum` FROM " . USERPREFIX . "_post
							LEFT JOIN " . USERPREFIX . "_post_extras ON " . USERPREFIX . "_post.id=" . USERPREFIX . "_post_extras.news_id
							WHERE " . USERPREFIX . "_post.category = '" . $row['id'] . "'" );	
		
		$rating = intval($ResultSum['sum']);


	
		$maps .= "
		
		var myPlacemark = new ymaps.Placemark([{$coord}], 
{
balloonContentHeader: \"<div class='maps-balloon'><table><tbody><tr><td class='details'><img src='/uploads/category/" . $row['alt_name'] . "/". $img. "'><div class='titles'><a href='{$row['alt_name']}' target='_top'>{$row['name']}</a></div><div class='address'>{$rating}</div>\" ,
					balloonContentBody: \"<div class='contacts'><li style='list-style: none;' class='phone'></li></ul></div></div></td></tr></tbody></table></div>\",
								},

{
					draggable: false,
					iconLayout: 'default#image',
					iconImageHref: '{THEME}/images/map.png',
					iconImageSize: [32, 37],
					iconImageOffset: [-3, -42]
				});

myGeoObjects.push(myPlacemark);
		";
		$i++;	
		
	}
	
$title = "Карта";
$metatags['title'] = $title;

$tpl->load_template('maps.tpl');
$db->free();
$tpl->set('{maps}',$maps);
$tpl->set('{first_point}',$first_point);
$tpl->compile('content');
$tpl->clear();

}

function cat3() {
global $db, $tpl, $metatags, $config, $lang, $is_logged, $member_id, $user_group;

$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_parentid2 IN (3) ORDER BY id");
$i=0;

while ($row = $db->get_row($tb))
	{
		$List[] = $row;
	}	

foreach($List as $row){

		$get_coor = $homepage = file_get_contents('https://geocode-maps.yandex.ru/1.x/?apikey=0e041a51-3941-4731-8143-3e8b36d4c073&format=json&geocode=' . urlencode( $row['ca_adress'] ) . '&results=1');
	
		$decode_coor = json_decode( $get_coor, true );
		
		$coord = $decode_coor['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
		$coord_de = explode(' ', $coord);
		$coord = "{$coord_de[1]}, {$coord_de[0]}";
		
		if( ! $first_point ) $first_point = $coord;
		
		/*подгружаем фотографии категории*/
		$cdir = scandir(ROOT_DIR . "/uploads/category/".$row['alt_name']); 
	   
		foreach ($cdir as $key => $value) 
		{ 
		  if (!in_array($value,array(".","..","Thumbs.db"))) 
		  { 
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
			 { 
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			 } 
			 else 
			 { 
				$result[] = $value; 
			 } 
		  } 
		} 
		
		$kid = 1;
		$previmg = '';
		
		foreach( $result as $img )
		{
			$exname = explode('.', $img);
			
			if( $exname[0] == 'main' )
			{
				//$tpl = str_replace('{image-1}', '/uploads/category/' . $row['alt_name'] . '/' . $img );
			}
			
			$kid += 1;
			
			
			if( $kid >= 1 )
			{
				$previmg .= '<img src="/uploads/category/' . $row['alt_name'] . '/' . $img . '" alt="">';
			}
		}
		
		# Рейтинг категории
		#
		$ResultSum = $db->super_query( "SELECT sum(rating) as `sum` FROM " . USERPREFIX . "_post
							LEFT JOIN " . USERPREFIX . "_post_extras ON " . USERPREFIX . "_post.id=" . USERPREFIX . "_post_extras.news_id
							WHERE " . USERPREFIX . "_post.category = '" . $row['id'] . "'" );	
		
		$rating = intval($ResultSum['sum']);


	
		$maps .= "
		
		var myPlacemark = new ymaps.Placemark([{$coord}], 
{
balloonContentHeader: \"<div class='maps-balloon'><table><tbody><tr><td class='details'><img src='/uploads/category/" . $row['alt_name'] . "/". $img. "'><div class='titles'><a href='{$row['alt_name']}' target='_top'>{$row['name']}</a></div><div class='address'>{$rating}</div>\" ,
					balloonContentBody: \"<div class='contacts'><li style='list-style: none;' class='phone'></li></ul></div></div></td></tr></tbody></table></div>\",
								},

{
					draggable: false,
					iconLayout: 'default#image',
					iconImageHref: '{THEME}/images/map.png',
					iconImageSize: [32, 37],
					iconImageOffset: [-3, -42]
				});

myGeoObjects.push(myPlacemark);
		";
		$i++;	
		
	}
	
$title = "Карта";
$metatags['title'] = $title;

$tpl->load_template('maps.tpl');
$db->free();
$tpl->set('{maps}',$maps);
$tpl->set('{first_point}',$first_point);
$tpl->compile('content');
$tpl->clear();

}

function cat4() {
global $db, $tpl, $metatags, $config, $lang, $is_logged, $member_id, $user_group;

$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_parentid3 IN (4) ORDER BY id");
$i=0;

while ($row = $db->get_row($tb))
	{
		$List[] = $row;
	}	

foreach($List as $row){

		$get_coor = $homepage = file_get_contents('https://geocode-maps.yandex.ru/1.x/?apikey=0e041a51-3941-4731-8143-3e8b36d4c073&format=json&geocode=' . urlencode( $row['ca_adress'] ) . '&results=1');
	
		$decode_coor = json_decode( $get_coor, true );
		
		$coord = $decode_coor['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
		$coord_de = explode(' ', $coord);
		$coord = "{$coord_de[1]}, {$coord_de[0]}";
		
		if( ! $first_point ) $first_point = $coord;
		
		/*подгружаем фотографии категории*/
		$cdir = scandir(ROOT_DIR . "/uploads/category/".$row['alt_name']); 
	   
		foreach ($cdir as $key => $value) 
		{ 
		  if (!in_array($value,array(".","..","Thumbs.db"))) 
		  { 
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
			 { 
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			 } 
			 else 
			 { 
				$result[] = $value; 
			 } 
		  } 
		} 
		
		$kid = 1;
		$previmg = '';
		
		foreach( $result as $img )
		{
			$exname = explode('.', $img);
			
			if( $exname[0] == 'main' )
			{
				//$tpl = str_replace('{image-1}', '/uploads/category/' . $row['alt_name'] . '/' . $img );
			}
			
			$kid += 1;
			
			
			if( $kid >= 1 )
			{
				$previmg .= '<img src="/uploads/category/' . $row['alt_name'] . '/' . $img . '" alt="">';
			}
		}
		
		# Рейтинг категории
		#
		$ResultSum = $db->super_query( "SELECT sum(rating) as `sum` FROM " . USERPREFIX . "_post
							LEFT JOIN " . USERPREFIX . "_post_extras ON " . USERPREFIX . "_post.id=" . USERPREFIX . "_post_extras.news_id
							WHERE " . USERPREFIX . "_post.category = '" . $row['id'] . "'" );	
		
		$rating = intval($ResultSum['sum']);


	
		$maps .= "
		
		var myPlacemark = new ymaps.Placemark([{$coord}], 
{
balloonContentHeader: \"<div class='maps-balloon'><table><tbody><tr><td class='details'><img src='/uploads/category/" . $row['alt_name'] . "/". $img. "'><div class='titles'><a href='{$row['alt_name']}' target='_top'>{$row['name']}</a></div><div class='address'>{$rating}</div>\" ,
					balloonContentBody: \"<div class='contacts'><li style='list-style: none;' class='phone'></li></ul></div></div></td></tr></tbody></table></div>\",
								},

{
					draggable: false,
					iconLayout: 'default#image',
					iconImageHref: '{THEME}/images/map.png',
					iconImageSize: [32, 37],
					iconImageOffset: [-3, -42]
				});

myGeoObjects.push(myPlacemark);
		";
		$i++;	
		
	}
	
$title = "Карта";
$metatags['title'] = $title;

$tpl->load_template('maps.tpl');
$db->free();
$tpl->set('{maps}',$maps);
$tpl->set('{first_point}',$first_point);
$tpl->compile('content');
$tpl->clear();

}

function cat5() {
global $db, $tpl, $metatags, $config, $lang, $is_logged, $member_id, $user_group;

$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_parentid4 IN (5) ORDER BY id");
$i=0;

while ($row = $db->get_row($tb))
	{
		$List[] = $row;
	}	

foreach($List as $row){

		$get_coor = $homepage = file_get_contents('https://geocode-maps.yandex.ru/1.x/?apikey=0e041a51-3941-4731-8143-3e8b36d4c073&format=json&geocode=' . urlencode( $row['ca_adress'] ) . '&results=1');
	
		$decode_coor = json_decode( $get_coor, true );
		
		$coord = $decode_coor['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
		$coord_de = explode(' ', $coord);
		$coord = "{$coord_de[1]}, {$coord_de[0]}";
		
		if( ! $first_point ) $first_point = $coord;
		
		/*подгружаем фотографии категории*/
		$cdir = scandir(ROOT_DIR . "/uploads/category/".$row['alt_name']); 
	   
		foreach ($cdir as $key => $value) 
		{ 
		  if (!in_array($value,array(".","..","Thumbs.db"))) 
		  { 
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
			 { 
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			 } 
			 else 
			 { 
				$result[] = $value; 
			 } 
		  } 
		} 
		
		$kid = 1;
		$previmg = '';
		
		foreach( $result as $img )
		{
			$exname = explode('.', $img);
			
			if( $exname[0] == 'main' )
			{
				//$tpl = str_replace('{image-1}', '/uploads/category/' . $row['alt_name'] . '/' . $img );
			}
			
			$kid += 1;
			
			
			if( $kid >= 1 )
			{
				$previmg .= '<img src="/uploads/category/' . $row['alt_name'] . '/' . $img . '" alt="">';
			}
		}
		
		# Рейтинг категории
		#
		$ResultSum = $db->super_query( "SELECT sum(rating) as `sum` FROM " . USERPREFIX . "_post
							LEFT JOIN " . USERPREFIX . "_post_extras ON " . USERPREFIX . "_post.id=" . USERPREFIX . "_post_extras.news_id
							WHERE " . USERPREFIX . "_post.category = '" . $row['id'] . "'" );	
		
		$rating = intval($ResultSum['sum']);


	
		$maps .= "
		
		var myPlacemark = new ymaps.Placemark([{$coord}], 
{
balloonContentHeader: \"<div class='maps-balloon'><table><tbody><tr><td class='details'><img src='/uploads/category/" . $row['alt_name'] . "/". $img. "'><div class='titles'><a href='{$row['alt_name']}' target='_top'>{$row['name']}</a></div><div class='address'>{$rating}</div>\" ,
					balloonContentBody: \"<div class='contacts'><li style='list-style: none;' class='phone'></li></ul></div></div></td></tr></tbody></table></div>\",
								},

{
					draggable: false,
					iconLayout: 'default#image',
					iconImageHref: '{THEME}/images/map.png',
					iconImageSize: [32, 37],
					iconImageOffset: [-3, -42]
				});

myGeoObjects.push(myPlacemark);
		";
		$i++;	
		
	}
	
$title = "Карта";
$metatags['title'] = $title;

$tpl->load_template('maps.tpl');
$db->free();
$tpl->set('{maps}',$maps);
$tpl->set('{first_point}',$first_point);
$tpl->compile('content');
$tpl->clear();

}

function cat6() {
global $db, $tpl, $metatags, $config, $lang, $is_logged, $member_id, $user_group;

$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_parentid5 IN (6) ORDER BY id");
$i=0;

while ($row = $db->get_row($tb))
	{
		$List[] = $row;
	}	

foreach($List as $row){

		$get_coor = $homepage = file_get_contents('https://geocode-maps.yandex.ru/1.x/?apikey=0e041a51-3941-4731-8143-3e8b36d4c073&format=json&geocode=' . urlencode( $row['ca_adress'] ) . '&results=1');
	
		$decode_coor = json_decode( $get_coor, true );
		
		$coord = $decode_coor['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
		$coord_de = explode(' ', $coord);
		$coord = "{$coord_de[1]}, {$coord_de[0]}";
		
		if( ! $first_point ) $first_point = $coord;
		
		/*подгружаем фотографии категории*/
		$cdir = scandir(ROOT_DIR . "/uploads/category/".$row['alt_name']); 
	   
		foreach ($cdir as $key => $value) 
		{ 
		  if (!in_array($value,array(".","..","Thumbs.db"))) 
		  { 
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
			 { 
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			 } 
			 else 
			 { 
				$result[] = $value; 
			 } 
		  } 
		} 
		
		$kid = 1;
		$previmg = '';
		
		foreach( $result as $img )
		{
			$exname = explode('.', $img);
			
			if( $exname[0] == 'main' )
			{
				//$tpl = str_replace('{image-1}', '/uploads/category/' . $row['alt_name'] . '/' . $img );
			}
			
			$kid += 1;
			
			
			if( $kid >= 1 )
			{
				$previmg .= '<img src="/uploads/category/' . $row['alt_name'] . '/' . $img . '" alt="">';
			}
		}
		
		# Рейтинг категории
		#
		$ResultSum = $db->super_query( "SELECT sum(rating) as `sum` FROM " . USERPREFIX . "_post
							LEFT JOIN " . USERPREFIX . "_post_extras ON " . USERPREFIX . "_post.id=" . USERPREFIX . "_post_extras.news_id
							WHERE " . USERPREFIX . "_post.category = '" . $row['id'] . "'" );	
		
		$rating = intval($ResultSum['sum']);


	
		$maps .= "
		
		var myPlacemark = new ymaps.Placemark([{$coord}], 
{
balloonContentHeader: \"<div class='maps-balloon'><table><tbody><tr><td class='details'><img src='/uploads/category/" . $row['alt_name'] . "/". $img. "'><div class='titles'><a href='{$row['alt_name']}' target='_top'>{$row['name']}</a></div><div class='address'>{$rating}</div>\" ,
					balloonContentBody: \"<div class='contacts'><li style='list-style: none;' class='phone'></li></ul></div></div></td></tr></tbody></table></div>\",
								},

{
					draggable: false,
					iconLayout: 'default#image',
					iconImageHref: '{THEME}/images/map.png',
					iconImageSize: [32, 37],
					iconImageOffset: [-3, -42]
				});

myGeoObjects.push(myPlacemark);
		";
		$i++;	
		
	}
	
$title = "Карта";
$metatags['title'] = $title;

$tpl->load_template('maps.tpl');
$db->free();
$tpl->set('{maps}',$maps);
$tpl->set('{first_point}',$first_point);
$tpl->compile('content');
$tpl->clear();

}

function cat7() {
global $db, $tpl, $metatags, $config, $lang, $is_logged, $member_id, $user_group;

$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_parentid6 IN (7) ORDER BY id");
$i=0;

while ($row = $db->get_row($tb))
	{
		$List[] = $row;
	}	

foreach($List as $row){

		$get_coor = $homepage = file_get_contents('https://geocode-maps.yandex.ru/1.x/?apikey=0e041a51-3941-4731-8143-3e8b36d4c073&format=json&geocode=' . urlencode( $row['ca_adress'] ) . '&results=1');
	
		$decode_coor = json_decode( $get_coor, true );
		
		$coord = $decode_coor['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
		$coord_de = explode(' ', $coord);
		$coord = "{$coord_de[1]}, {$coord_de[0]}";
		
		if( ! $first_point ) $first_point = $coord;
		
		/*подгружаем фотографии категории*/
		$cdir = scandir(ROOT_DIR . "/uploads/category/".$row['alt_name']); 
	   
		foreach ($cdir as $key => $value) 
		{ 
		  if (!in_array($value,array(".","..","Thumbs.db"))) 
		  { 
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
			 { 
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			 } 
			 else 
			 { 
				$result[] = $value; 
			 } 
		  } 
		} 
		
		$kid = 1;
		$previmg = '';
		
		foreach( $result as $img )
		{
			$exname = explode('.', $img);
			
			if( $exname[0] == 'main' )
			{
				//$tpl = str_replace('{image-1}', '/uploads/category/' . $row['alt_name'] . '/' . $img );
			}
			
			$kid += 1;
			
			
			if( $kid >= 1 )
			{
				$previmg .= '<img src="/uploads/category/' . $row['alt_name'] . '/' . $img . '" alt="">';
			}
		}
		
		# Рейтинг категории
		#
		$ResultSum = $db->super_query( "SELECT sum(rating) as `sum` FROM " . USERPREFIX . "_post
							LEFT JOIN " . USERPREFIX . "_post_extras ON " . USERPREFIX . "_post.id=" . USERPREFIX . "_post_extras.news_id
							WHERE " . USERPREFIX . "_post.category = '" . $row['id'] . "'" );	
		
		$rating = intval($ResultSum['sum']);


	
		$maps .= "
		
		var myPlacemark = new ymaps.Placemark([{$coord}], 
{
balloonContentHeader: \"<div class='maps-balloon'><table><tbody><tr><td class='details'><img src='/uploads/category/" . $row['alt_name'] . "/". $img. "'><div class='titles'><a href='{$row['alt_name']}' target='_top'>{$row['name']}</a></div><div class='address'>{$rating}</div>\" ,
					balloonContentBody: \"<div class='contacts'><li style='list-style: none;' class='phone'></li></ul></div></div></td></tr></tbody></table></div>\",
								},

{
					draggable: false,
					iconLayout: 'default#image',
					iconImageHref: '{THEME}/images/map.png',
					iconImageSize: [32, 37],
					iconImageOffset: [-3, -42]
				});

myGeoObjects.push(myPlacemark);
		";
		$i++;	
		
	}
	
$title = "Карта";
$metatags['title'] = $title;

$tpl->load_template('maps.tpl');
$db->free();
$tpl->set('{maps}',$maps);
$tpl->set('{first_point}',$first_point);
$tpl->compile('content');
$tpl->clear();

}

function cat8() {
global $db, $tpl, $metatags, $config, $lang, $is_logged, $member_id, $user_group;

$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_parentid7 IN (8) ORDER BY id");
$i=0;

while ($row = $db->get_row($tb))
	{
		$List[] = $row;
	}	

foreach($List as $row){

		$get_coor = $homepage = file_get_contents('https://geocode-maps.yandex.ru/1.x/?apikey=0e041a51-3941-4731-8143-3e8b36d4c073&format=json&geocode=' . urlencode( $row['ca_adress'] ) . '&results=1');
	
		$decode_coor = json_decode( $get_coor, true );
		
		$coord = $decode_coor['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
		$coord_de = explode(' ', $coord);
		$coord = "{$coord_de[1]}, {$coord_de[0]}";
		
		if( ! $first_point ) $first_point = $coord;
		
		/*подгружаем фотографии категории*/
		$cdir = scandir(ROOT_DIR . "/uploads/category/".$row['alt_name']); 
	   
		foreach ($cdir as $key => $value) 
		{ 
		  if (!in_array($value,array(".","..","Thumbs.db"))) 
		  { 
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
			 { 
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			 } 
			 else 
			 { 
				$result[] = $value; 
			 } 
		  } 
		} 
		
		$kid = 1;
		$previmg = '';
		
		foreach( $result as $img )
		{
			$exname = explode('.', $img);
			
			if( $exname[0] == 'main' )
			{
				//$tpl = str_replace('{image-1}', '/uploads/category/' . $row['alt_name'] . '/' . $img );
			}
			
			$kid += 1;
			
			
			if( $kid >= 1 )
			{
				$previmg .= '<img src="/uploads/category/' . $row['alt_name'] . '/' . $img . '" alt="">';
			}
		}
		
		# Рейтинг категории
		#
		$ResultSum = $db->super_query( "SELECT sum(rating) as `sum` FROM " . USERPREFIX . "_post
							LEFT JOIN " . USERPREFIX . "_post_extras ON " . USERPREFIX . "_post.id=" . USERPREFIX . "_post_extras.news_id
							WHERE " . USERPREFIX . "_post.category = '" . $row['id'] . "'" );	
		
		$rating = intval($ResultSum['sum']);


	
		$maps .= "
		
		var myPlacemark = new ymaps.Placemark([{$coord}], 
{
balloonContentHeader: \"<div class='maps-balloon'><table><tbody><tr><td class='details'><img src='/uploads/category/" . $row['alt_name'] . "/". $img. "'><div class='titles'><a href='{$row['alt_name']}' target='_top'>{$row['name']}</a></div><div class='address'>{$rating}</div>\" ,
					balloonContentBody: \"<div class='contacts'><li style='list-style: none;' class='phone'></li></ul></div></div></td></tr></tbody></table></div>\",
								},

{
					draggable: false,
					iconLayout: 'default#image',
					iconImageHref: '{THEME}/images/map.png',
					iconImageSize: [32, 37],
					iconImageOffset: [-3, -42]
				});

myGeoObjects.push(myPlacemark);
		";
		$i++;	
		
	}
	
$title = "Карта";
$metatags['title'] = $title;

$tpl->load_template('maps.tpl');
$db->free();
$tpl->set('{maps}',$maps);
$tpl->set('{first_point}',$first_point);
$tpl->compile('content');
$tpl->clear();

}

function cat9() {
global $db, $tpl, $metatags, $config, $lang, $is_logged, $member_id, $user_group;

$sql_result = $db->query("SELECT * FROM ".PREFIX."_category WHERE ca_parentid8 IN (9) ORDER BY id");
$i=0;

while ($row = $db->get_row($tb))
	{
		$List[] = $row;
	}	

foreach($List as $row){

		$get_coor = $homepage = file_get_contents('https://geocode-maps.yandex.ru/1.x/?apikey=0e041a51-3941-4731-8143-3e8b36d4c073&format=json&geocode=' . urlencode( $row['ca_adress'] ) . '&results=1');
	
		$decode_coor = json_decode( $get_coor, true );
		
		$coord = $decode_coor['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
		$coord_de = explode(' ', $coord);
		$coord = "{$coord_de[1]}, {$coord_de[0]}";
		
		if( ! $first_point ) $first_point = $coord;
		
		/*подгружаем фотографии категории*/
		$cdir = scandir(ROOT_DIR . "/uploads/category/".$row['alt_name']); 
	   
		foreach ($cdir as $key => $value) 
		{ 
		  if (!in_array($value,array(".","..","Thumbs.db"))) 
		  { 
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
			 { 
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			 } 
			 else 
			 { 
				$result[] = $value; 
			 } 
		  } 
		} 
		
		$kid = 1;
		$previmg = '';
		
		foreach( $result as $img )
		{
			$exname = explode('.', $img);
			
			if( $exname[0] == 'main' )
			{
				//$tpl = str_replace('{image-1}', '/uploads/category/' . $row['alt_name'] . '/' . $img );
			}
			
			$kid += 1;
			
			
			if( $kid >= 1 )
			{
				$previmg .= '<img src="/uploads/category/' . $row['alt_name'] . '/' . $img . '" alt="">';
			}
		}
		
		# Рейтинг категории
		#
		$ResultSum = $db->super_query( "SELECT sum(rating) as `sum` FROM " . USERPREFIX . "_post
							LEFT JOIN " . USERPREFIX . "_post_extras ON " . USERPREFIX . "_post.id=" . USERPREFIX . "_post_extras.news_id
							WHERE " . USERPREFIX . "_post.category = '" . $row['id'] . "'" );	
		
		$rating = intval($ResultSum['sum']);


	
		$maps .= "
		
		var myPlacemark = new ymaps.Placemark([{$coord}], 
{
balloonContentHeader: \"<div class='maps-balloon'><table><tbody><tr><td class='details'><img src='/uploads/category/" . $row['alt_name'] . "/". $img. "'><div class='titles'><a href='{$row['alt_name']}' target='_top'>{$row['name']}</a></div><div class='address'>{$rating}</div>\" ,
					balloonContentBody: \"<div class='contacts'><li style='list-style: none;' class='phone'></li></ul></div></div></td></tr></tbody></table></div>\",
								},

{
					draggable: false,
					iconLayout: 'default#image',
					iconImageHref: '{THEME}/images/map.png',
					iconImageSize: [32, 37],
					iconImageOffset: [-3, -42]
				});

myGeoObjects.push(myPlacemark);
		";
		$i++;	
		
	}
	
$title = "Карта";
$metatags['title'] = $title;

$tpl->load_template('maps.tpl');
$db->free();
$tpl->set('{maps}',$maps);
$tpl->set('{first_point}',$first_point);
$tpl->compile('content');
$tpl->clear();

}


$category = !empty($_POST['category']) ? $_POST['category'] : $_GET['category'];

switch ($category) {

case "main":
        main();
        break;

case "cat1":
        cat1();
        break;

case "cat2":
        cat2();
        break;
		
case "cat3":
        cat3();
        break;
		
case "cat4":
        cat4();
        break;
		
case "cat5":
        cat5();
        break;
		
case "cat6":
        cat6();
        break;
		
case "cat7":
        cat7();
        break;
		
case "cat8":
        cat8();
        break;
		
case "cat9":
        cat9();
        break;
	
default:
        main();
        break;

}

?>